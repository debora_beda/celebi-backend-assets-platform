import { deleteItem } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function deleteAssets(request, response) {
  logger.info('Delete item');

  const params = {
    Key: {
      "id":  `${request.params.id}`
    },
    TableName: "celebi-assets"
  };

  try {
    const result = await deleteItem(params);
    console.log(result);

    if(result.ConsumedCapacity) {
      response.json({ message: 'Not Found' });
    } else {
      response.json({ message: 'Asset successfully deleted' });
    }
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not delete assets' });
  }
}

export { deleteAssets };