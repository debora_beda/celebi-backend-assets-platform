import { deleteItem } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function deleteMaintenance(request, response) {
  logger.info('Delete maintenance');

  const params = {
    Key: {
      "id":  `${request.params.id}`
    },
    TableName: "celebi-maintenance"
  };

  try {
    const result = await deleteItem(params);
    console.log(result);

    if(result.ConsumedCapacity) {
      response.json({ message: 'Not Found' });
    } else {
      response.json({ message: 'Maintenance successfully deleted' });
    }
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not delete mintenance' });
  }
}

export { deleteMaintenance };