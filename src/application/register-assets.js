import { v4 as uuidv4 } from 'uuid';
import { put } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function registerAssets(request, response) {
  logger.info('Register item');

  const id = uuidv4();

  const params = {
    TableName: 'celebi-assets',
    Item: {
      id: id,
      payload: request.body
    }
  };

  try {
    await put(params);
    response.json({ id });
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not register an asset' });
  }
}

export { registerAssets };