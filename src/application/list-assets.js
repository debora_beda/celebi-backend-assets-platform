import { scan } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function listAssets(request, response) {
  logger.info('List item');

  const params = {
    TableName: 'celebi-assets',
  };

  try {
    const result = await scan(params);
    response.json(result);
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not list assets' });
  }
}

export { listAssets };