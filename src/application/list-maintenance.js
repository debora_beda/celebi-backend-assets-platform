import { scan } from '../infrastructure/dynamodb';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function listMaintenance(request, response) {
  logger.info('List maintenance');

  const params = {
    TableName: 'celebi-maintenance',
  };

  try {
    const result = await scan(params);
    response.json(result);
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could not list maintenance' });
  }
}

export { listMaintenance };