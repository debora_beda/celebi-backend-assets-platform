import serverless from 'serverless-http';
import app from './infrastructure/server/server';

module.exports.handler = serverless(app);