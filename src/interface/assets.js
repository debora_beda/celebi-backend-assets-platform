import Router from 'express';
import { registerAssets } from '../application/register-assets';
import { listAssets } from '../application/list-assets';
import { deleteAssets } from '../application/delete-assets';
import { serviceLogger } from '../infrastructure/config/service-logger';

const assetRoutes = Router();
const logger = serviceLogger.getInstance();

assetRoutes.get("/assets", async (request, response) => {
  logger.info('message get')
  await listAssets(request, response);
})

assetRoutes.post("/assets", async (request, response) => {
  logger.info('message post')
  await registerAssets(request, response);
})

assetRoutes.delete("/assets/:id", async (request, response) => {
  await deleteAssets(request, response);
})

export default assetRoutes;