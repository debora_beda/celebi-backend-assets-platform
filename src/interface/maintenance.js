import Router from 'express';
import { registerMaintenance } from '../application/register-maintenance';
import { listMaintenance } from '../application/list-maintenance';
import { deleteMaintenance } from '../application/delete-maintenance';

const maintenanceRoutes = Router();

maintenanceRoutes.get("/maintenance", async (request, response) => {
  await listMaintenance(request, response);
})

maintenanceRoutes.post("/maintenance", async (request, response) => {
  await registerMaintenance(request, response);
})

maintenanceRoutes.delete("/maintenance/:id", async (request, response) => {
  await deleteMaintenance(request, response);
})

export default maintenanceRoutes;