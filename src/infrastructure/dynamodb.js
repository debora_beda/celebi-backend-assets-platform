import * as AWS from 'aws-sdk';

async function put(params) {
  let dynamodb;

  if (process.env.IS_OFFLINE) {
    dynamodb = new AWS.DynamoDB.DocumentClient({
      region: 'localhost',
      endpoint: 'http://localhost:8000',
      accessKeyId: 'DEFAULT_ACCESS_KEY',  // needed if you don't have aws credentials at all in env
      secretAccessKey: 'DEFAULT_SECRET' // needed if you don't have aws credentials at all in env
    });
  } else {
    dynamodb = new AWS.DynamoDB.DocumentClient({
      region: 'us-east-1',
    });
  }

  return dynamodb.put(params).promise();
}

async function scan(params) {
  let dynamodb;

  if (process.env.IS_OFFLINE) {
    dynamodb = new AWS.DynamoDB.DocumentClient({
      region: 'localhost',
      endpoint: 'http://localhost:8000',
      accessKeyId: 'DEFAULT_ACCESS_KEY',  // needed if you don't have aws credentials at all in env
      secretAccessKey: 'DEFAULT_SECRET' // needed if you don't have aws credentials at all in env
    });
  } else {
    dynamodb = new AWS.DynamoDB.DocumentClient({
      region: 'us-east-1',
    });
  }

  return dynamodb.scan(params).promise();
}

async function deleteItem(params) {
  let dynamodb;

  if (process.env.IS_OFFLINE) {
    dynamodb = new AWS.DynamoDB.DocumentClient({
      region: 'localhost',
      endpoint: 'http://localhost:8000',
      accessKeyId: 'DEFAULT_ACCESS_KEY',  // needed if you don't have aws credentials at all in env
      secretAccessKey: 'DEFAULT_SECRET' // needed if you don't have aws credentials at all in env
    });
  } else {
    dynamodb = new AWS.DynamoDB.DocumentClient({
      region: 'us-east-1',
    });
  }

  return dynamodb.delete(params).promise();
}

export { put, scan, deleteItem };